function rand(m, n) {
  return m + Math.floor((n - m + 1) * Math.random());
}

function randFace() {
  const faces = ['crown', 'anchor', 'heart', 'spade', 'club', 'diamond'];

  return faces[rand(0, 5)];
}

let funds = 50;
let round = 0;

while (funds > 0 && funds < 100) {
  round++;
  console.log(`Round ${round}:`);
  console.log(`\tStarting funds: ${funds}p`);

  // place bets
  let bets = {
    crown: 0,
    anchor: 0,
    heart: 0,
    spade: 0,
    club: 0,
    diamond: 0
  };
  let totalBet = rand(1, funds);
  if (totalBet === 7) {
    totalBet = funds;
    bets.heart = totalBet;
  } else {
    let remaining = totalBet;
    do {
      let bet = rand(1, remaining);
      let face = randFace();
      bets[face] = bets[face] + bet;
      remaining = remaining - bet;
    } while (remaining > 0);
  }
  funds = funds - totalBet;
  console.log(
    '\tbets: ' +
      Object.keys(bets)
        .map(face => `${face}: ${bets[face]}p`)
        .join(', ') +
      ` (total: ${totalBet}p)`
  );

  // roll dice
  let hand = [];
  for (let roll = 0; roll < 3; roll++) {
    hand.push(randFace());
  }
  console.log(`\thand: ${hand.join(', ')}`);

  // collect winnings
  let winnings = 0;
  for (let i = 0; i < hand.length; i++) {
    let face = hand[i];
    if (bets[face] > 0) {
      winnings = winnings + bets[face];
    }
  }
  funds = funds + winnings;
  console.log(`\twinnings: ${winnings}`);
}
console.log(`\tending funds: ${funds}`);
