const ActiveDirectory = require("activedirectory");
const adConfig = {
  url: "ldap://medway.nhs.uk",
  baseDN: "DC=medway,DC=nhs,DC=uk",
  username: "svc_ironpond",
  password: "Hospital1"
};
const ad = new ActiveDirectory(adConfig);

function getUser(username) {
  let availability;

  return new Promise(function (resolve, reject) {
    ad.findUser(username, function (error, user) {
      availability = user ? false : true;

      resolve(availability);
    });
  });
}

let int

async function createAdUsername(username) {
  if (await getUser(username)) {
    return username
  }
}

// username = createAdUsername("tony.leafdsfd");
console.log(createAdUsername("tony.leafdsfd"))
