const ActiveDirectory = require("activedirectory");
const adConfig = {
  url: "ldap://medway.nhs.uk",
  baseDN: "DC=medway,DC=nhs,DC=uk",
  username: "svc_ironpond",
  password: "Hospital1"
};
const ad = new ActiveDirectory(adConfig);

function isUsernameAvailable(samAccountName) {
  let availability;
  return new Promise(function(resolve, reject) {
    ad.findUser(samAccountName, function(error, user) {
      availability = samAccountName ? false : true;
      resolve(availability);
    });
  });
}

isUsernameAvailable("tony.lea").then(function(result) {
  return console.log(result);
});
