//import ActiveDirectory from 'activedirectory';
const ActiveDirectory = require("activedirectory");
const adConfig = {
    url: 'ldap://medway.nhs.uk',
    baseDN: 'DC=medway,DC=nhs,DC=uk',
    username: 'svc_ironpond',
    password: 'Hospital1'
};
const ad = new ActiveDirectory(adConfig);
let int = ''

function isUsernameAvailable(samAccountName) {
    let availability = false;
    console.log(`Checking SAMACCOUNTNAME ${samAccountName}`)
    return new Promise(function (resolve, reject) {
        ad.findUser(samAccountName, function (error, user) {
            availability = samAccountName ? false : true;
            resolve(availability);
        });
    });
}

async function createAdUsername(username) {
    const usernameWithInt = `${username.toLowerCase()}${int}`;
    console.log(`Checking username ${usernameWithInt}`)
    int++
    const isAvailable = await isUsernameAvailable(usernameWithInt);
    console.log(isAvailable)
    if (!isAvailable) {
        createAdUsername(username)
    } else {
        return usernameWithInt
    }

}

console.log(createAdUsername('tony.lea'))

// export default createAdUsername;
